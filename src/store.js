import Vuex from 'Vuex';
import Vue from 'vue';


Vue.use(Vuex);
const mutations={
    set(state,data)
    {
        state.data=data;
    }
};
const state={
    data:[]
         };
       
        const  getters={
                getData : state =>{
                    return state.data;
                }
               };
export default  new Vuex.Store({
    state , mutations , getters
    });