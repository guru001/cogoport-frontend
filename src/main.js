import Vue from 'vue';
import Default from './App.vue';
import Search from './search.vue';
import Games from './games.vue';
import Sort from './sort.vue';
import Item from './details.vue';
import Zircle from 'zircle';
import 'zircle/dist/zircle.min.css'
import { log } from 'util';
import Store from './store';
import Vuex from 'Vuex';
import 'materialize-css/sass/materialize.scss';
Vue.use(Zircle);


new Vue({
  beforeMount () 
  {

    this.$zircleStore.setView('Default');

  },
  el: '#app', store:Store,
components:{
Default,
Search,
Games,
Sort,
Item
}
})
